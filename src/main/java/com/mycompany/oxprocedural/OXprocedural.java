/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */
package com.mycompany.oxprocedural;

import java.util.Scanner;

/**
 *
 * @author NonWises
 */
public class OXprocedural {

    static char table[][] = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    static char currentPlayer = 'O';
    static Scanner kb = new Scanner(System.in);
    static int row, col;
    static boolean finish = false;
    static int count = 0;

    public static void main(String[] args) {

        showWelcome();
        while (true) {
            showTable(row, col);
            showTurn(currentPlayer);
            inputRowCol();
            process();
            if (finish) {
                break;
            }
        }

    }

    public static void showTable(int row, int col) {
        for ( row = 0; row < table.length; row++) {
            for ( col = 0; col < table[row].length; col++) {
                System.out.print(table[row][col]);
            }
            System.out.println("");
        }
    }

    public static void showWelcome() {
        System.out.println("Welcome to OX Game");
    }

    public static void showTurn(char currentplayer) {
        System.out.println("Turn " + currentPlayer);
    }

    public static void inputRowCol() {
        while (true) {
            System.out.println("Please input row, col:");
            row = kb.nextInt();
            col = kb.nextInt();
            if (checkOutOfRange(row, col)) continue;
            if (checkDuplicate(row, col)) continue;
            break;

        }

    }

    public static boolean checkDuplicate(int row, int col) {
        if ((getIndex(row - 1, col - 1) == 'O') || (getIndex(row - 1, col - 1) == 'X')) {
            System.out.println("This index is already taken, please try again.");
            return true;
        }
        return false;
    }

    public static boolean checkOutOfRange(int row, int col) {
        if (((row <= 0) || (row > 3)) || ((col <= 0) || (col > 3))) {
            System.out.println("Index out of range, please input in [1,2,3]");
            return true;
        }
        return false;
    }

    public static void process() {
        if (setTable()) {
            if (checkWin(row,col ,currentPlayer)) {
                finish = true;
                showWin(row, col,currentPlayer );
                return;
            }
            
            count++;
            if (checkDraw(count)) {
                finish = true;
                System.out.println(finish);
                showDraw(row, col);
                return;
            }
            System.out.println(count);
            switchPlayer();
        }

    }

    public static void showWin(int row,int col, char currentPlayer) {
        showTable(row,col);
        System.out.println(">>>" + currentPlayer + " Win<<<");
    }

    public static void switchPlayer() {
        if (currentPlayer == 'O') {
            currentPlayer = 'X';
        } else {
            currentPlayer = 'O';
        }
    }

    public static boolean setTable() {
        table[row - 1][col - 1] = currentPlayer;
        return true;
    }

    public static boolean checkWin(int col, int row, char currentPlayer) {
        if (checkVertical(col)) {
            return true;
        } else if (checkHorizontal(row)) {
            return true;
        } else if (checkDiagonal(currentPlayer)) {
            return true;
        }

        return false;
    }

    public static boolean checkVertical(int col) {
        for (int r = 0; r < table.length; r++) {
            if (table[r][col - 1] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    public static boolean checkHorizontal(int row) {
        for (int c = 0; c < table.length; c++) {
            if (table[row - 1][c] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    public static boolean checkDiagonal(char currentPlayer) {
        if (checkLeftDiagonal(currentPlayer)) {
            return true;
        } else if (checkRightDiagonal(currentPlayer)) {
            return true;
        }
        return false;
    }

    public static boolean checkLeftDiagonal(char currentPlayer) {
        for (int i = 0; i < table.length; i++) {
            if (table[i][i] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    public static boolean checkRightDiagonal(char currentPlayer) {
        for (int i = 0; i < table.length; i++) {
            if (table[i][2 - i] != currentPlayer) {
                return false;
            }
        }
        return false;
    }

    public static boolean checkDraw(int count) {
        if(count == 9) {
            return true;
        }
        return false;
    }

    public static void showDraw(int row, int col) {
        showTable(row, col);
        System.out.println(">>>Draw<<<");
    }

    public static char getIndex(int a, int b) {
        return table[a][b];
    }
}
