/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */

package com.mycompany.oxprocedural;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author NonWises
 */
public class OXprogramTest {
    
    @Test
    public void testCheckVertical() {
        OXprocedural.currentPlayer = 'O';
        OXprocedural.table[0][0] =  'O';
        OXprocedural.table[1][0] =  'O';
        OXprocedural.table[2][0] =  'O';
        //OXprocedural.col= 1;
        
        assertEquals(true,OXprocedural.checkVertical(1));
}
    @Test
    public void testCheckHorizontal() {
        OXprocedural.currentPlayer = 'O';
        OXprocedural.table[0][0] =  'O';
        OXprocedural.table[0][1] =  'O';
        OXprocedural.table[0][2] =  'O';
        
        assertEquals(true,OXprocedural.checkHorizontal(1));
    }
    @Test
    public void testCheckDiagonal() {
        OXprocedural.currentPlayer = 'O';
        OXprocedural.table[0][0] =  'O';
        OXprocedural.table[1][1] =  'O';
        OXprocedural.table[2][2] =  'O';
        
        assertEquals(true,OXprocedural.checkDiagonal('O'));
    }
    @Test
    public void testCheckDraw() {
        OXprocedural.count = 9;
        
        assertEquals(true,OXprocedural.checkDraw(9));
    }
    
    
   

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
}
